# section TODO list
## General
* [ ] Overview

## User guide
* [ ] How to run the UI
* [ ] How to use devices / nodes
* [ ] Simple Manager
* [ ] Advanced Manager

## Developer guide
* [ ] Architecture and technologies
* [ ] Project structure
* [ ] Adding modes
* [ ] Other development things
