# section TODO list
## General
* [ ] Overview
* [ ] Architecture
* [ ] History

## User guidance
* [ ] User guide  
  (what sections are recommended and what should be skipped)

## Developer guidance
* [ ] Developer guide

## General other things
* [ ] Credits and special shout-outs
* [ ] Pictures
