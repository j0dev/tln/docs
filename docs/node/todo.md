# section TODO list
## General
* [ ] Overview
* [ ] What is it
* [ ] How does it work (simple overview)

## Development guide
* [ ] Architecture
* [ ] How does it work internally
* [ ] Development environment and building
* [ ] Developing modes / effects
* [ ] Generated class / interface documentation?
* [ ] Link to communication API
