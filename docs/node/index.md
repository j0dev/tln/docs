# TLN Node Overview
Here you will find all the documentation related to the Nodes in the system.

## What is / does the node
The node represents the controller that physically controls individual lights.  
This is the place where the modes / effects are run to let the lights do something.  
Normally there will be one controller / node per light (led grid/strip).
