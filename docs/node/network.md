# Network Management (WIP)
This page contains all information to properly manage your nodes network configuration


## Overview
To make network management easier, the [ESP NetworkManager](https://gitlab.com/j0dev/arduino/networkmanager) library is used.

If you are already familiar with this library, you may skip all but our overridden defaults outlined in [Network Configuration](#network-configuration).  
If you are deploying to a board that already has network configuration for this library, it will **not** be overridden and will be used, so this method can be used for pre-provisioning or recovery if wanted.

If you are not yet familiar with this library, it is not necessary to read their entire documentation outside of what we link to. but feel free to read it if you are interested.

## Network Configuration
If your already familiar with the library, only the default `hostname prefix` and default `AP password` are overridden. Also no restrictions are made to what can be configured.


### Summary
Default AP SSID:&nbsp; `TLN-{mac-address}`  
Default AP Pass:&nbsp; `TimesLightNetwork`  
Default IP:&nbsp; `192.168.4.1`  
Default Hostname: `TLN-{mac-address}`  
Default Webserver port:&nbsp; `80`

### General settings

#### Hostname
By default the hostname takes the form of a `prefix` and the `mac-address` of the board.  
Our prefix is set to `TLN-`.  
So the hostname should be `TLN-{mac-address}`


### Access Point Mode (AP)
By default the node will provide a WiFi Access Point that you can connect to.  
You can use this mode to control the node or to configure it to join another network using the [network management web interface](#network-management-web-interface).

#### Network Name
The default network name of the WiFi Access Point is the same as the hostname of the device:  
`TLN-{mac-address}`

#### Network Password
The password by default is set to `TimesLightNetwork` (case sensitive).  
You should probably change this especially when you plan on using this mode to control your node

#### IP Addressing
The IP addressing of the nodes will be the same as the default used by your board / platform.  
For ESP32 and ESP8266 boards this will be `192.168.4.1`.

#### Timeout and fallback mode
By default, no timeout is configured (yet).  
[ESP NetworkManager](https://gitlab.com/j0dev/arduino/networkmanager) does not yet have the option to have the timeout only work if other networks are configured.  
In the future this will be enabled once available.
Fallback mode also is not yet implemented, so more information will come when this is available.

The timeout option will automatically disable AP mode after a default of 60 seconds. The timeout will also (in the future) be automatically canceled when the network manager is opened.  
This functionality is highly recommended as a fallback for when no other connections are available or when the device has to be reconfigured, but a previously configured network is not available.

It is highly recommended to keep the AP mode activated and set a timeout when you do configure other network modes.


### WiFi Client Mode (STA)
By default this mode is disabled and has to be configured before it can be used.
See [network management web interface](#network-management-web-interface) section for how to configure this.


### Ethernet
By default this mode is disabled and has to be configured before it can be used.
This mode is currently not yet supported!


## Network Management Web Interface
Please see [ESP NetworkManager](https://gitlab.com/j0dev/arduino/networkmanager)'s user documentation.  
TODO direct link


## Network Recovery
Please see [ESP NetworkManager](https://gitlab.com/j0dev/arduino/networkmanager)'s recovery documentation.  
TODO direct link


## Network Provisioning
As outlined above, no options are currently limited so pre-provisioning all options are supported.  
Please see [ESP NetworkManager](https://gitlab.com/j0dev/arduino/networkmanager)'s provisioning documentation.  
TODO direct link
